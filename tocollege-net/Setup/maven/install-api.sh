mvn install:install-file -DgroupId=com.google -DartifactId=gwt-incubator -Dversion=0.0.1.build591 -Dpackaging=jar -Dfile=gwt-incubator-0.0.1.build591.jar -DgeneratePom=true
mvn install:install-file -DgroupId=com.google -DartifactId=gwt-google-apis -Dversion=1.5.0.build235 -Dpackaging=jar -Dfile=gwt-google-apis-1.5.0.build235.jar -DgeneratePom=true
mvn install:install-file -DgroupId=com.allen_sauer.gwt.dnd -DartifactId=gwt-dnd -Dversion=2.0.7 -Dpackaging=jar -Dfile=gwt-dnd-2.0.7.jar -DgeneratePom=true
mvn install:install-file -DgroupId=com.allen_sauer.gwt.log -DartifactId=gwt-log -Dversion=1.5.6 -Dpackaging=jar -Dfile=gwt-log-1.5.6.jar -DgeneratePom=true
mvn install:install-file -DgroupId=javax.transaction -DartifactId=jta -Dversion=1.0.1B -Dpackaging=jar -Dfile=jta-1.0.1B.jar -DgeneratePom=true
mvn install:install-file -DgroupId=org.opensymphony -DartifactId=compass -Dversion=1.2.0 -Dpackaging=jar -Dfile=compass-1.2.0.jar -DgeneratePom=true
mvn install:install-file -DgroupId=com.sun.xml -DartifactId=jaxb-impl -Dversion=2.1 -Dpackaging=jar -Dfile=jaxb-impl-2.1.jar -DgeneratePom=true
mvn install:install-file -DgroupId=com.facebook.api -DartifactId=facebook-java-api -Dversion=1.7.4 -Dpackaging=jar -Dfile=facebook-java-api-1.7.4.jar -DgeneratePom=true

cd ../..
mvn clean install
